### This repository is deprecated. The services have been split off into their own repositories:

- [gateway](https://gitlab.com/helfa-web/gateway)
- [mongodb](https://gitlab.com/helfa-web/mongo-db)

---

# @helfa/docker

Docker deployment for helfa.org

### Install

```
git clone gitlab:helfa-web/docker helfa.org
```

1. Install services with `./manage setup`.
2. Add `.secrets/postgres/.env.dev` with `POSTGRES_DB`, `POSTGRES_USER` and `POSTGRES_PASSWORD`.
3. Edit `.secrets/api/.env.dev` and change
  - `DB_HOST` (postgres),
  - `DB_USER`,
  - `DB_PASSWORD`
  - `PUBLIC_URL` (`https://api.helfa.me`)
5. Create an SSL certificate for `api.helfa.me` and `helfa.me` with `./manage mkcert <domain>`
4. Start services with `./manage up`
6. Open `https://helfa.me` and `https://api.helfa.me` in your browser.
