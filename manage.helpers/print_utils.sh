print_error() {
  echo -e "$COLOR_FG_RED!! $1$COLOR_RESET"
}
print_warning() {
  echo -e "${COLOR_FG_YELLOW}$1$COLOR_RESET"
}
print_success() {
  echo -e "${COLOR_FG_GREEN}$1$COLOR_RESET"
}

print_banner_border() {
  printf '#%.0s' `seq $1`; printf '\n'
}

print_banner() {
  local TITLE="$1 $COLOR_FG_CYAN$2$COLOR_RESET"
  local TITLE_LENGTH=${#TITLE}
  local DIVIDER_LENGTH=$(($TITLE_LENGTH + 4))

  echo ""
  print_banner_border $DIVIDER_LENGTH
  echo -e "# $TITLE"
  print_banner_border $DIVIDER_LENGTH
}

# Print help and usage info
# $1: Highlight/explain an invalid option
# $2: Highlight/explain a missing option
print_help() {
  echo "Usage: ./manage COMMAND [OPTIONS] [-h] [-b <branch>] [-p]"
  echo -e "Setup and manage helfa.org deployment\n"
  echo "Depends:"
  echo -e "  docker >=19.03.0, docker-compose >=1.25\n"

  if [ -n "$1" ]; then
    echo -e "$COLOR_FG_YELLOW--- Invalid option: -$1 ---$COLOR_RESET\n"
  fi
  if [ -n "$2" ]; then
    echo -e "$COLOR_FG_YELLOW--- Missing option: $2 ---$COLOR_RESET\n"
  fi

  echo "Managing commands:"
  #echo "  backup                    Creates a backup of the postgres database in ./backups/postgres"
  echo "  help                      Show help; same as -h"
  echo "  list <service>            List all available services or info about"
  echo "                            a service when <service> is given."
  echo "  mkcert <domain>           Creates a new SSL cert/key pair for <domain>"
  echo "  reset                     Deletes all services in ./services"
  echo "  setup <service>           Clone and setup services into $LOCALS_BASE."
  echo "                            Sets up all defined services when <service> is omitted."
  echo "Options:"
  echo "  -b <branch>               Set branch to clone/checkout in service setup (default: $BRANCH)"
  echo "  -d <domain>               Set the domain name to use (default: $DOMAIN)"
  echo "  -h                        You're looking at it."
  echo "  -p                        Run commands for production."
  echo "  *                         All other arguments are passed to docker-compose."

  exit
}

bail_on_fail() {
  if [ $1 -ne 0 ]; then
    print_error "Can't proceed without $2. Please check above output for errors!"
    exit $1
  fi
}

