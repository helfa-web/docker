check_package_manager() {
  [ $PACKAGE_MANAGER ] && return
  print_banner "?" "Checking package manager"
  # preferring yarn
  yarn -v &> /dev/null
  local YARN_RESULT=$?

  if [ $YARN_RESULT -eq 0 ]; then
    PACKAGE_MANAGER="yarn"
    PACKAGE_MANGER_INSTALL_CMD="yarn"
  else
    # at least npm is a must
    npm -v &> /dev/null
    local NPM_RESULT=$?

    [ $NPM_RESULT -ne 0 ] && \
      print_error "Neither 'yarn' nor 'npm' found. Is one of them in your PATH?"
    bail_on_fail $NPM_RESULT "yarn or npm"

    PACKAGE_MANAGER="npm"
    PACKAGE_MANGER_INSTALL_CMD="npm i"
  fi

  echo -e "${COLOR_FG_GREEN}using $PACKAGE_MANAGER$COLOR_RESET"
}

check_version() {
  local TESTED_BINARY=$1

  local VERSION=$2
  local VERSION_MAJOR=$((`echo $VERSION | cut -d '.' -f1`))
  local VERSION_MINOR=$((`echo $VERSION | cut -d '.' -f2`))

  local MIN_VERSION_MAJOR=$3
  local MIN_VERSION_MINOR=$4
  local MIN_VERSION="$MIN_VERSION_MAJOR.$MIN_VERSION_MINOR"

  local CHECK_FAILED=0

  echo $TESTED_BINARY
  echo "  Required: $MIN_VERSION"
  echo -ne "  Installed: "

  [ $VERSION_MAJOR -lt $MIN_VERSION_MAJOR ] && \
    CHECK_FAILED=1

  [ $VERSION_MAJOR -eq $MIN_VERSION_MAJOR ] && \
    [ $VERSION_MINOR -lt $MIN_VERSION_MINOR ] && \
      CHECK_FAILED=1

  if [ $CHECK_FAILED -eq 1 ]; then
    echo -e "$COLOR_FG_RED$VERSION$COLOR_RESET"
    print_error "Please upgrade to or install $TESTED_BINARY v$MIN_VERSION or later!"
    exit 1
  fi

  echo -e "$COLOR_FG_GREEN$VERSION$COLOR_RESET"
}

check_docker_compose_version() {
  local MIN_VERSION_MAJOR=1
  local MIN_VERSION_MINOR=25
  local VERSION=`docker-compose version --short`

  check_version \
    'docker-compose' \
    $VERSION \
    $MIN_VERSION_MAJOR \
    $MIN_VERSION_MINOR
}

check_docker_version() {
  local MIN_VERSION_MAJOR=19
  local MIN_VERSION_MINOR=03
  local VERSION=`docker version --format '{{.Server.Version}}'`

  check_version \
    'docker-compose' \
    $VERSION \
    $MIN_VERSION_MAJOR \
    $MIN_VERSION_MINOR
}

check_docker_versions() {
  print_banner "?" "Checking Docker versions"
  check_docker_version
  check_docker_compose_version
}
